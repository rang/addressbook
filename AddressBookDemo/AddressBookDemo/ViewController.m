//
//  ViewController.m
//  AddressBookDemo
//
//  Created by wulanzhou on 2017/5/10.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

#import "ViewController.h"
#import "KBAddressBookManager.h"
#import "CacheAddressBook.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//联系人加载完成通知
- (void)receiveContactLoadFinished:(NSNotification *)notify{

    NSLog(@"联系人加载完成!");
    if ([KBAddressBookManager shareManger].allContacts && [[KBAddressBookManager shareManger].allContacts count]>0) {
        
        [[CacheAddressBook shareManger] cacheContacts];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismissWithDelay:1.0f];
            self.labMessage.text = @"联系人备份成功";
        });
        
        
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismissWithDelay:1.0f];
           self.labMessage.text = @"联系人备份失败";
        });
        
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KBAddressBookLoadCompeted object:nil];
}

//获取联系人权限
- (IBAction)powerClick:(id)sender {
    [[KBAddressBookManager shareManger] canReadAddressBookStatusBlock:^(BOOL canRead, NSInteger status) {
        if(canRead){
            NSLog(@"可读");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // something
                self.labMessage.text = @"获取联系人权限";
            });
        }else{
            NSLog(@"不可读");
            dispatch_async(dispatch_get_main_queue(), ^{
               self.labMessage.text = @"未获取联系人权限";
            });
            
        }
    }];
}

//加载联系人
- (IBAction)loadClick:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    [[KBAddressBookManager shareManger] loadContactsCompletion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismissWithDelay:1.0f];
            self.labMessage.text = @"联系人加载完成";
        });
    }];
}

//备份联系人
- (IBAction)backupClick:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    if ([KBAddressBookManager shareManger].allContacts && [[KBAddressBookManager shareManger].allContacts count]>0) {
        [[CacheAddressBook shareManger] cacheContacts];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismissWithDelay:1.0f];
            self.labMessage.text = @"联系人备份成功";
        });
        
    }else{
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveContactLoadFinished:) name:KBAddressBookLoadCompeted object:nil];
        
       [[KBAddressBookManager shareManger] loadContactsCompletion:nil];
    }
}

//恢复联系人
- (IBAction)restoreClick:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    
    NSArray *arr =[[CacheAddressBook shareManger] getCacheContacts];
    if (arr && [arr count]>0) {
        
        [[KBAddressBookManager shareManger] restoreContacts:arr];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismissWithDelay:1.0f];
            self.labMessage.text = @"联系人恢复成功";
        });
    }else{
       
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismissWithDelay:1.0f];
            self.labMessage.text = @"联系人恢复失败";
        });
    }
}

//清空联系人
- (IBAction)clearEmptyClick:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD show];
    });
    [[KBAddressBookManager shareManger] clearContacts];
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismissWithDelay:1.0f];
        self.labMessage.text = @"联系人清空成功";
    });
}
@end
