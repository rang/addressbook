//
//  CacheAddressBook.h
//  AddressBookDemo
//
//  Created by wulanzhou on 2017/5/11.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CacheAddressBook : NSObject

+ (instancetype)shareManger;

- (void)cacheContacts;
- (id)getCacheContacts;

@end
