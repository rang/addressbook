//
//  ContactNode.h
//  AddressBookDemo
//
//  Created by wulanzhou on 2017/5/10.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <Contacts/Contacts.h>

@interface ContactNode : NSObject

@property(nonatomic,strong) NSString              *contactID;         //联系人ID，同通讯录中的personID
@property(nonatomic,strong) NSString              *firstName;
@property(nonatomic,strong) NSString              *middleName;
@property(nonatomic,strong) NSString              *lastName;
@property(nonatomic,strong) NSString              *firstPinyin;    //firstName拼音
@property(nonatomic,strong) NSString              *middlepinyin;   //middleName拼音
@property(nonatomic,strong) NSString              *lastPinyin;     //lastName拼音
@property(nonatomic,strong) NSString              *nickName;
@property(nonatomic,strong) NSString              *companyName;    //公司名称
@property(nonatomic,strong) NSDate                *birthday;       //生日
@property(nonatomic,strong) NSData                *imageData;       //头像
@property(nonatomic,strong) NSArray               *phoneList;

- (instancetype)initContactNodeWithRecord:(ABRecordRef)onePerson;
- (instancetype)initContactNodeWithContact:(CNContact *)contact;

- (NSString *)getContactFullName;
- (NSString *)getContactSortKey;

@end
