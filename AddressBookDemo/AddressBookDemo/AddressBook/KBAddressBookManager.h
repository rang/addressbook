//
//  KBAddressBookManager.h
//  AddressBookDemo
//
//  Created by wulanzhou on 2017/5/10.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ContactNode;

UIKIT_EXTERN NSString *const  KBAddressBookLoadCompeted; //联系人加载完成

typedef void(^KBAddressBookCanReadBlock)(BOOL canRead, NSInteger status);
typedef void(^KBAddressBookLoadFinishedBlock)();

@interface KBAddressBookManager : NSObject

@property (nonatomic,strong,readonly) NSDictionary *allContacts;
@property (nonatomic,strong,readonly) NSArray *sortKeys;

/**
 单例
 
 @return KBAddressBookManager
 */
+ (instancetype)shareManger;

/**
 通读录权限设置

 @param block 通读录权限回调
 */
- (void)canReadAddressBookStatusBlock:(KBAddressBookCanReadBlock)block;

/**
 读取联系人
 */
- (void)loadContactsCompletion:(KBAddressBookLoadFinishedBlock)completion;

/**
 清空联系人
 */
- (void)clearContacts;

/**
 恢复联系人

 @param contacts 联系人集合
 */
- (void)restoreContacts:(NSArray <ContactNode *> *)contacts;

@end
