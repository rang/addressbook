//
//  ViewController.h
//  AddressBookDemo
//
//  Created by wulanzhou on 2017/5/10.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labMessage;
- (IBAction)powerClick:(id)sender;
- (IBAction)loadClick:(id)sender;
- (IBAction)backupClick:(id)sender;
- (IBAction)restoreClick:(id)sender;
- (IBAction)clearEmptyClick:(id)sender;


@end

